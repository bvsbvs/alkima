<!-- Header -->
<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-heading text-uppercase">Individuelle Weblösungen</div>
            <div class="intro-lead-in">Für anspruchsvolle Anforderungen</div>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Mehr erfahren</a>
        </div>
    </div>
</header>