<!-- Footer -->
<footer>
      <div class="container">
        <div class="row">
          <div class="col-md-6 text-left">
            <span class="copyright">
              <p>
                ALKIMA Weblösungen</br>
                Geschäftsführer: Marco Puppe, Kian Foroodi</br>
                Colmarer Str. 12</br>
                65203 Wiesbaden</br>
                E-Mail: <a href="mailto:info@alkima.de">info@alkima.de</a></br>
                Tel: <a href="tel:+4961194586400">+49 611 9458 64 00</a>
              </p>
            </span>
          </div>
          <!--div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div-->
          <div class="col-md-6">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="datenschutz.html">Datenschutz</a>
              </li>
              <li class="list-inline-item">
                <a href="#" class="cookie-settings-link">Cookie-Einstellungen</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
